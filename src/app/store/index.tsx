import { combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { IVideo } from './video/types';
import { videoReducer } from './video/reducer';

interface IRootState {
	video: IVideo;
}
const store = createStore<IRootState, any, any, any>(combineReducers({ video: videoReducer }), composeWithDevTools());

export default store;
