import { ActionType } from 'typesafe-actions';
import * as actions from './actions';

export type VideoActions = ActionType<typeof actions>;
export interface IHotSpot {
	image?: string;
	time: number;
	description?: string;
}

export interface IVideo {
	url: string;
	isPlayed: boolean;
	time: number;
	hotspots?: IHotSpot[];
	duration: number;
}

export enum Constants {
	SET_URL = 'SET_URL',
	SET_IS_PLAYED = 'SET_IS_PLAYED',
	SET_IS_PLAYABLE = 'SET_IS_PLAYABLE',
	SET_TIME = 'SET_TIME',
	SET_DURATION = 'SET_DURATION',
	ADD_HOTSPOTS = 'ADD_HOTSPOTS'
}
