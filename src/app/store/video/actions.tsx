import { action } from 'typesafe-actions';
import { Constants, IHotSpot } from './types';

/*
* Returns action that sets url of video
*@param {string} url
*@return
*
*/
export function setUrl(url: string) {
	return action(Constants.SET_URL, {
		url
	});
}

/*
* Returns action that sets time of video
*@param {number} time
*@return
*
*/
export function setTime(time: number) {
	return action(Constants.SET_TIME, {
		time
	});
}

/*
* Returns action that determine if video is currently played
*@param {boolean} isPlayed
*@return
*
*/
export function setPlayed(isPlayed: boolean) {
	return action(Constants.SET_IS_PLAYED, {
		isPlayed
	});
}

/*
* Returns action that sets length of video
*@param {number} duration
*@return
*
*/
export function setDuration(duration: number) {
	return action(Constants.SET_DURATION, {
		duration
	});
}

/*
* Returns action that returns array Hotspots
*@param {IHotSpot[]} hotspots
*@return
*
*/
export function addHotspots(hotspots: IHotSpot[]) {
	return action(Constants.ADD_HOTSPOTS, {
		hotspots
	});
}
