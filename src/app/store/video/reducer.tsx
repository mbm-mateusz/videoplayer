import { Constants, VideoActions, IVideo } from './types';

const init: IVideo = {
	url:
		'https://cdn.videvo.net/videvo_files/video/free/2019-03/small_watermarked/190111_08_BuildingsTraffic_Drone_18_preview.webm',
	isPlayed: false,
	time: 0,
	duration: 0
};

export function videoReducer(state: IVideo = init, action: VideoActions) {
	switch (action.type) {
		case Constants.SET_URL:
			return { ...state, url: action.payload.url };
		case Constants.SET_TIME:
			return { ...state, time: action.payload.time };
		case Constants.SET_IS_PLAYED:
			return { ...state, isPlayed: action.payload.isPlayed };
		case Constants.SET_DURATION:
			return { ...state, duration: action.payload.duration };
		case Constants.ADD_HOTSPOTS:
			return { ...state, hotspots: action.payload.hotspots };
		default:
			return state;
	}
}
