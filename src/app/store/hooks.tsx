import store from './';
import * as actions from './video/actions';
import { IHotSpot } from './video/types';

export const getStore = {
	video: () => store.getState().video
};

export const useDispatch = {
	setUrl: (value: string) => store.dispatch(actions.setUrl(value)),
	setTime: (value: number) => store.dispatch(actions.setTime(value)),
	setPlayed: (value: boolean) => store.dispatch(actions.setPlayed(value)),
	setDuration: (value: number) => store.dispatch(actions.setDuration(value)),
	addHotspots: (value: IHotSpot[]) => store.dispatch(actions.addHotspots(value))
};
