import React, { useEffect, useRef, useState } from 'react';
import { getStore, useDispatch } from '../../../store/hooks';
import './Video.scss';
import store from '../../../store';

const Video: React.FC<{ time?: number }> = ({ time }) => {
	const url = getStore.video().url;
	const videoRef = useRef<HTMLVideoElement>(null);
	const [ isPlayed, setPlayed ] = useState(getStore.video().isPlayed);
	const link = time ? `${url}#t=${time}` : url;
	useEffect(() => {
		store.subscribe(() => {
			if (videoRef.current !== null)
				if (getStore.video().time !== videoRef.current.currentTime)
					videoRef.current.currentTime = getStore.video().time;
		});
		store.subscribe(() => {
			if (videoRef.current)
				if (getStore.video().isPlayed !== isPlayed) {
					setPlayed(!isPlayed);
				}
		});
		isPlayed ? videoRef.current.play() : videoRef.current.pause();
		videoRef.current.addEventListener('timeupdate', () => {
			if (videoRef.current) useDispatch.setTime(videoRef.current.currentTime);
		});
		videoRef.current.addEventListener('loadeddata', () => {
			if (videoRef.current) useDispatch.setDuration(videoRef.current.duration);
		});
	});
	return (
			<video ref={videoRef} preload='metadata' className='video'>
				<source src={link} />
			</video>
	);
};

export default Video;
