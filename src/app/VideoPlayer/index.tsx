import React, { useRef, useState } from 'react';
import './VideoPlayer.scss';
import { Provider } from 'react-redux';
import { Switch, Route, useLocation } from 'react-router-dom';
import store from '../store';
import { useDispatch, getStore } from '../store/hooks';
import { IHotSpot } from '../store/video/types';
import Video from './VideoFrame/Video';
import Controller from './Controller';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

export interface IVideoPlayer {
	url: string;
	hotspots?: IHotSpot[];
}

const VideoPlayer: React.FC<IVideoPlayer> = ({ url, hotspots}) => {
	useDispatch.setUrl(url);
	useDispatch.addHotspots(hotspots);
	return (
		<Provider store={store}>
			<div className='videoframe'>
				<div className='videoframe__inner'>
					<TransitionGroup style={{ width: '100%', display: 'flex' }}>
						<CSSTransition key={useLocation().key} timeout={{ enter: 400, exit: 400 }} classNames='fade'>
							<Switch location={useLocation()}>
								<Route exact path='/' component={() => <Video time={0} />} />
								{hotspots? hotspots.map((el, index) => {
									return (
										<Route
											component={() => <Video time={el.time} />}
											path={'/' + el.time.toString()}
											key={'route_' + index}
										/>
									);
								}):null}
							</Switch>
						</CSSTransition>
					</TransitionGroup>
					<Controller />
				</div>
			</div>
		</Provider>
	);
};

export default VideoPlayer;
