import React, {useRef, useEffect} from 'react'
import store from '../../../store'
import {useDispatch, getStore} from '../../../store/hooks'
import './PlayButton.scss'

const PlayButton: React.FC = () => {
    const buttonRef = useRef<HTMLButtonElement>()
    const switchIcon = () => {
        if(getStore.video().isPlayed){
            buttonRef.current.classList.remove('play')
            buttonRef.current.classList.add('pause')
        }else{ 
            buttonRef.current.classList.remove('pause')
            buttonRef.current.classList.add('play')
        }
    }
    useEffect(()=>{
        store.subscribe(()=>{
            switchIcon()
        })
    })
    const handlePlay = () => {
        useDispatch.setPlayed(!getStore.video().isPlayed)
        switchIcon()
    }
    return(
        <button 
            className="playbutton play"
            ref={buttonRef}
            onClick={handlePlay}
            >
        </button>
    )
}

export default PlayButton