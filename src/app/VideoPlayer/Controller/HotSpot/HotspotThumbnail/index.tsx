import React from 'react';
import { getStore } from '../../../../store/hooks';
import './thumbnail.scss';
import { IHotSpot } from '../../../../store/video/types';

const HotspotThumbnail: React.FC<IHotSpot> = ({ image, time, description }) => {
	const left = Math.floor(time / getStore.video().duration * 100);
	return (
		<div style={{ marginLeft: left - 12 + '%' }} className='thumbnail'>
			{image ? (
				<img className='videoshot' src={image} />
			) : (
				<video
					src={getStore.video().url + `#t=` + time}
					controls={false}
					preload='auto'
					className='videoshot'
				/>
			)}
			<p className='thumbnail__description'>{description || 'no description'}</p>
		</div>
	);
};

export default HotspotThumbnail;
