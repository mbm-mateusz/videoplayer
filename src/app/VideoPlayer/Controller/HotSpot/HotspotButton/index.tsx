import React from 'react';
import { Link } from "react-router-dom";
import { IHotSpot } from '../../../../store/video/types';
import { getStore, useDispatch } from '../../../../store/hooks';
import './HotSpot.scss';

interface IHotspotButton extends IHotSpot {
	onMouseEnter?: any;
	onMouseLeave?: any;
}
const HotspotButton: React.FC<IHotspotButton> = ({ time, onMouseEnter, onMouseLeave }) => {
	const left = Math.floor(time / getStore.video().duration * 100);
	const handleHover = () => {
		onMouseEnter ? onMouseEnter() : null
		useDispatch.setPlayed(false)
	}
	const handleUnHover = () => {
		onMouseLeave ? onMouseLeave() :null
	}
	return (
		<Link
			className='hotspot'
			onMouseEnter={handleHover}
			onMouseLeave={handleUnHover}
			to={'/'+time}
			style={{ marginLeft: left + '%' }}
		/>
	);
};

export default HotspotButton;
