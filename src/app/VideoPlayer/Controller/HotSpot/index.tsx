import React, {useState, useEffect, useRef} from 'react'
import { IHotSpot } from '../../../store/video/types';
import HotspotThumbnail from './HotspotThumbnail'
import HotspotButton from './HotspotButton'

const HotSpot: React.FC<IHotSpot> = ({image, description, time}) => {
    const [visible, setVisible] = useState(false)
    const thumbRef = useRef<HTMLDivElement>(null)

    useEffect(()=>{
        visible ? thumbRef.current.style.opacity = '1' : thumbRef.current.style.opacity = '0'
    }, [visible])

    return(
        <>
        <HotspotButton onMouseEnter={()=>setVisible(true)} onMouseLeave={()=>setVisible(false)} image={image} description={description} time={time}/>
        <div ref={thumbRef} className="thumbnail__outter">
        <HotspotThumbnail image={image} description={description} time={time}/>
        </div>
        </>
    )
}

export default HotSpot