import React, { useRef, useState } from 'react';
import PlayButton from './PlayButton';
import TimeLine from './TimeLine';
import './Controller.scss';

const Controller: React.FC = () => {
	const [ hovered, setHovered ] = useState(false);
	return (
		<div
			className='controller'
			onMouseEnter={() => setHovered(true)}
			onMouseLeave={() => setHovered(false)}
			style={hovered ? { opacity: 1 } : { opacity: 0 }}
		>
			<div className='controller__inner'>
				<PlayButton />
				<TimeLine />
			</div>
		</div>
	);
};

export default Controller;
