import React, { useRef, useEffect, useState } from 'react';
import { getStore, useDispatch } from '../../../store/hooks';
import HotSpot from '../HotSpot';
import './TimeLine.scss';
import store from '../../../store';
const TimeLine: React.FC = () => {
	const rangeRef = useRef<HTMLInputElement>();
	const [ value, setValue ] = useState('0');

	useEffect(() => {
		store.subscribe(() => {
			setValue(Math.floor(getStore.video().time / getStore.video().duration * 100).toString());
			rangeRef.current.value = value;
			rangeRef.current.style.background = `linear-gradient(90deg, rgb(255,255,255) ${value}%,rgba(255, 255, 255, 0.2) ${value}%)`;
		});
	});

	const changeTime = () => {
		useDispatch.setTime(rangeRef.current.valueAsNumber * getStore.video().duration / 100);
	};
	return (
		<div className='timeline'>
			<input
				onClick={changeTime}
				type='range'
				min='0'
				max='100'
				className='timeline__inner'
				step={1}
				ref={rangeRef}
			/>
			{getStore.video().hotspots ? getStore.video().hotspots.map((el, index) => {
				return <HotSpot time={el.time} key={'hotspot_'+index} description={el.description} image={el.image} />;
			}):null}
		</div>
	);
};

export default TimeLine;
