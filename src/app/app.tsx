import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import VideoPlayer from './VideoPlayer';
import './index.scss';

const hotsoptArr = [ { time: 2, description: 'Lorum ipsumum' }, { time: 80 }, {time:300}, {time:600, image:'https://img2.grunge.com/img/gallery/the-most-disturbing-thing-about-the-spanish-inquisition-isnt-what-you-think/everyone-expected-the-spanish-inquisition-1544042193.jpg'}];
const App:React.FC = () => {
	return (
		<Router>
		<VideoPlayer
			hotspots={hotsoptArr}
			url='https://ia801405.us.archive.org/9/items/teletubbiesheydiddlediddleusversion/Teletubbies%20Hey%20Diddle%20Diddle%20%28US%20Version%29.mp4'
		/>
		</Router>
	);
};

export default App