import React from 'react';
import ReactDOM from 'react-dom';
import TimeLine from '../app/VideoPlayer/Controller/TimeLine';
import { expect } from 'chai';
import { act } from 'react-dom/test-utils';
import { fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from '../app/store';
import * as actions from '../app/store/video/actions';

describe('<HotspotThumbnail />', () => {
	let container;
	beforeEach(() => {
		container = document.createElement('div');
		document.body.appendChild(container);
		act(() => {
			ReactDOM.render(
				<Provider store={store}>
					<TimeLine />
				</Provider>,
				container
			);
		});
		store.dispatch(actions.setUrl(''));
		store.dispatch(actions.setDuration(10));
		store.dispatch(actions.setTime(5));
	});
	it('should progress', () => {
		expect(container.querySelector('.timeline__inner').value).to.equal('50');
	});
});
