import React from 'react';
import ReactDOM from 'react-dom';
import HotspotThumbnail from '../app/VideoPlayer/Controller/HotSpot/HotspotThumbnail';
import { expect } from 'chai';
import { act } from 'react-dom/test-utils';

describe('<HotspotThumbnail />', () => {
    let container;
	beforeEach(() => {
		container = document.createElement('div');
        document.body.appendChild(container);
	});
	it('should have default text "no description"', () => {
        act(() => {
			ReactDOM.render(
                    <HotspotThumbnail time={3}/>
            , container);
		});
		expect(container.querySelector('p').innerHTML).to.equal('no description');
    });
	it('should display set description', () => {
        act(() => {
			ReactDOM.render(
                    <HotspotThumbnail time={3} description="abc"/>
            , container);
		});
		expect(container.querySelector('p').innerHTML).to.equal('abc');
    });
});