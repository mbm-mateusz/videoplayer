import React from 'react';
import ReactDOM from 'react-dom';
import HotSpot from '../app/VideoPlayer/Controller/HotSpot';
import { expect } from 'chai';
import { act } from 'react-dom/test-utils';
import {fireEvent} from "@testing-library/react";
import { BrowserRouter as Router } from 'react-router-dom';

describe('<Controller />', () => {
	let container;
	beforeEach(() => {
		container = document.createElement('div');
        document.body.appendChild(container);
        act(() => {
			ReactDOM.render(
                <Router>
                    <HotSpot time={3}/>
                </Router>
            , container);
		});
	});
	it('should not display thumbnail if not hover', () => {
		expect(container.querySelector('.thumbnail__outter').style.opacity).to.not.equal('1');
	});
	it('should display thumbnail if not hover', () => {
		fireEvent.mouseEnter(container.querySelector('.hotspot'))
		expect(container.querySelector('.thumbnail__outter').style.opacity).to.equal('1');
	});
});