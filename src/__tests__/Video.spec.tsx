import React from 'react';
import ReactDOM from 'react-dom'
import Video from '../app/VideoPlayer/VideoFrame/Video';
import { expect } from "chai";
import { act } from 'react-dom/test-utils';

describe("<Video />",()=>{
    let container;
    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
      });
    it("should render", ()=> {
        act(()=>{
            ReactDOM.render(
                    <Video />, container)
        })})
    it("should render video", ()=> {
        act(()=>{
            ReactDOM.render(
                    <Video />, container)
        })
        const video = document.querySelectorAll('video')
        expect(video.length).to.not.equal(-1)
    })
})