import React from 'react';
import ReactDOM from 'react-dom';
import PlayButton from '../app/VideoPlayer/Controller/PlayButton';
import { expect } from 'chai';
import { act } from 'react-dom/test-utils';
import {fireEvent} from "@testing-library/react";
import { Provider } from "react-redux";
import store from '../app/store'
import * as actions from '../app/store/video/actions';

describe('<HotspotThumbnail />', () => {
    let container;
	beforeEach(() => {
		container = document.createElement('div');
        document.body.appendChild(container);
        act(() => {
			ReactDOM.render(
                <Provider store={store}>
                    <PlayButton />
                </Provider>
            , container);
		});
	});
	it('should display pause icon on play', () => {
        store.dispatch(actions.setPlayed(true))
		expect(container.querySelector('.playbutton').classList.contains('pause')).to.equal(true);
    });
	it('should display play icon on pause', () => {
        store.dispatch(actions.setPlayed(false))
		expect(container.querySelector('.playbutton').classList.contains('play')).to.equal(true);
    });
	it('should switch icons on click', () => {
        store.dispatch(actions.setPlayed(false))
        fireEvent.click(container.querySelector('.playbutton'))
		expect(container.querySelector('.playbutton').classList.contains('pause')).to.equal(true);
    });
});