import React from 'react';
import ReactDOM from 'react-dom';
import HotspotButton from '../app/VideoPlayer/Controller/HotSpot/HotspotButton';
import { expect } from 'chai';
import { act } from 'react-dom/test-utils';
import {fireEvent} from "@testing-library/react";
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from "react-redux";
import store from '../app/store'
import * as actions from '../app/store/video/actions';

describe('<HotspotButton />', () => {
    let container;
	beforeEach(() => {
		container = document.createElement('div');
        document.body.appendChild(container);act(() => {
			ReactDOM.render(
                <Provider store={store}>
                <Router>
                    <HotspotButton time={3}/>
                </Router>
                </Provider>
            , container);
		});
	});
	it('should change url on click', () => {
		fireEvent.click(container.querySelector('.hotspot'))
		expect(window.location.pathname).to.equal('/3');
    });
	it('set store isPlayed false onMouseEnter', () => {
        store.dispatch(actions.setPlayed(true))
		fireEvent.mouseEnter(container.querySelector('.hotspot'))
		expect(store.getState().video.isPlayed).to.equal(false);
	});
});