import React from 'react';
import ReactDOM from 'react-dom';
import Controller from '../app/VideoPlayer/Controller';
import { expect } from 'chai';
import { act } from 'react-dom/test-utils';
import {fireEvent} from "@testing-library/react";

describe('<Controller />', () => {
	let container;
	beforeEach(() => {
		container = document.createElement('div');
		document.body.appendChild(container);
	});
	it('should render', () => {
		act(() => {
			ReactDOM.render(<Controller />, container);
		});
	});
	it('should be invisible if not hover', () => {
		act(() => {
			ReactDOM.render(<Controller />, container);
		});
		expect(container.querySelector('.controller').style.opacity).to.equal('0');
	});
	it('should be visible on hover', () => {
		act(() => {
			ReactDOM.render(<Controller />, container);
		});
		fireEvent.mouseEnter(container.querySelector('.controller'))
		expect(container.querySelector('.controller').style.opacity).to.equal('1');
	});
});
