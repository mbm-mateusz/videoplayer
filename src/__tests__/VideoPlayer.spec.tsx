import React from 'react';
import ReactDOM from 'react-dom'
import VideoPlayer from '../app/VideoPlayer';
import { expect } from "chai";
import { act } from 'react-dom/test-utils';
import { BrowserRouter as Router } from 'react-router-dom';

describe("<Router />", ()=>{
    let container;
    beforeEach(() => {
        container = document.createElement('div');
        document.body.appendChild(container);
      });
    it("should render", ()=> {
        act(()=>{
            ReactDOM.render(
                <Router>
                    <VideoPlayer 
                        url='https://cdn.videvo.net/videvo_files/video/free/2019-03/small_watermarked/190111_08_BuildingsTraffic_Drone_18_preview.webm'
                    />
                </Router>, container)
        })})
    it("should render video", ()=> {
        act(()=>{
            ReactDOM.render(
                <Router>
                    <VideoPlayer 
                        url='https://cdn.videvo.net/videvo_files/video/free/2019-03/small_watermarked/190111_08_BuildingsTraffic_Drone_18_preview.webm'
                    />
                </Router>, container)
        })
        const video = container.querySelectorAll('video')
        expect(video.length).to.not.equal(-1)
    })
    it("should render hotspots", ()=> {
        act(()=>{
            const hotspots=[{time:2},{time:3,description:'nana'}]
            ReactDOM.render(
                <Router>
                    <VideoPlayer 
                        url='https://cdn.videvo.net/videvo_files/video/free/2019-03/small_watermarked/190111_08_BuildingsTraffic_Drone_18_preview.webm'
                        hotspots={hotspots}
                    />
                </Router>, container)
        })
        const links = container.querySelectorAll('a')
        expect(links.length).to.equal(2)
    })
})