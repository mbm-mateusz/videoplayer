import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin';

module.exports = {
    entry:'./src/app/index.tsx',
    watch:true,
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'index.bundle.js'
	},
	devtool: 'source-map',
	resolve: {
		extensions: [ '.js', '.jsx', '.json', '.ts', '.tsx' ]
	},
	module: {
		rules: [
			
            { test: /\.scss$/, use: [ "style-loader", "css-loader", "sass-loader" ] },
            { test: /\.tsx?$/, loader: "babel-loader" },
            { test: /\.tsx?$/, loader: "ts-loader" },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
		]
	},
	plugins: [
		new HtmlWebpackPlugin({ template: path.resolve(__dirname, 'src', 'public', 'index.html') })
	]
};
