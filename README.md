# Videoplayer made with React 

**React** , **TypeScript** , **Redux** , **SCSS** , **webpack** , **jest** , **chai** , **enzyme**

## Instruction

Component is placed in 'app' directory and uses Redux store hidden in 'store' directory. 

Component REQUIRES url prop as string with url to mp4/webm video.
You can also set hostpots as array with objects, which require only time, but you can also set descripton, and image url (else the thumbnal will display relevant video frame, and "no description" text).

### Set up example:
```
<VideoPlayer
			hotspots={hotsoptArr}
			url='https://ia801405.us.archive.org/9/items/teletubbiesheydiddlediddleusversion/Teletubbies%20Hey%20Diddle%20Diddle%20%28US%20Version%29.mp4'
		/>
const hotsoptArr = [ { time: 2, description: 'Lorum ipsumum' }, { time: 80 }, {time:300}, {time:600, image:'https://img2.grunge.com/img/gallery/the-most-disturbing-thing-about-the-spanish-inquisition-isnt-what-you-think/everyone-expected-the-spanish-inquisition-1544042193.jpg'}];    
```
